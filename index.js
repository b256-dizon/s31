
// A. What directive is used by Node.js in loading the modules it needs?

// 		require("http");

// B. What Node.js module contains a method for server creation?

// 	"http"

// C. What is the method of the http object responsible for creating a server using Node.js?

// 	createServer()

// D. What method of the response object allows us to set status codes and content types?

// 	writeHead()

// E. Where will console.log() output its contents when run in Node.js?

// 	Git

// F. What property of the request object contains the address's endpoint?

// 	request.url == "theEndPointOfAddress" 


const http = require("http");

const port = 3000;

const server = http.createServer(function (request, response) {

	if(request.url == "/login"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("You have succesfully login!");
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("Page not found!")
	}
})

server.listen(port);

console.log(`Server successfully initiated at localhost:${port}`);
